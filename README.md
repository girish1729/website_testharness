# Website www.spamcheetah.com test harness #

This details how to test backend and URLs of the website.

### What is this repository for? ###

* This is the test suite for website running at  https://www.spamcheetah.com
* 1.0

### How do I get set up? ###

* Just use pytest
* It uses remote HTTP using requests library
* `pip install pytest pytest-pysugar pytest-repeat` 

## Standard tests

* All 3 get and 5 post methods of visitor\_actions
* One get and one post of mail\_test\_rw
* Two get for forgotPassword
* One post for search

### Invocation

* visitor\_actions  all actions excepting deploy\_instance invoked by
  website js

* deploy\_instance invoked by license perl CGI backend once user
 installs appliance after download in their network

* The invoice and user home stuff are inside AuthGuard block in UI

