import json
import pytest
import datetime as DT
from random import choice
import requests
import sys, os
from truth.truth import AssertThat

#######################################
#	Test all HTTP GET/POST endpoints 
#######################################

def getmailtest(PARAMS):
  resp = requests.get(
	"http://www.spamcheetah.com/cgi-bin/mail_test_rw",
   params=PARAMS, auth=('access','dance'))
  return(resp)


def getsearch(PARAMS):
  resp = requests.get(
	"http://www.spamcheetah.com/cgi-bin/search",
   params=PARAMS, auth=('access','dance'))
  return(resp)

def getforgot(PARAMS):
  resp = requests.get(
	"http://www.spamcheetah.com/cgi-bin/forgotPassword",
   params=PARAMS, auth=('access','dance'))
  return(resp)


def getvals(PARAMS):
  resp = requests.get(
	"http://www.spamcheetah.com/cgi-bin/visitor_actions",
   params=PARAMS, auth=('access','dance'))
  return(resp)

def postreq(body):
  resp = requests.post("http://spamcheetah.com/cgi-bin/visitor_actions",
   json=body, auth=('access','dance'))
  return(resp)
 
def postcred(body):
  resp = requests.post("http://spamcheetah.com/cgi-bin/userCredMgr",
   json=body, auth=('access','dance'))
  return(resp)

def postmailtest(body):
  resp = requests.post("http://spamcheetah.com/cgi-bin/mail_test_rw",
   json=body, auth=('access','dance'))
  return(resp)

#######################################
# XXX 
#######################################

# Visitor_actions
def test_get_invoices(capsys):
   resp = getvals({"q":"invoices"})
   json = resp.json()
   assert len(json) > 0
   print(resp.json())
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

# Visitor_actions
def test_get_bugreports(capsys):
   resp = getvals({"q":"bugreports"})
   print(resp.json())
   json = resp.json()
   assert len(json) > 0
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

# Visitor_actions
def test_get_supportreqs(capsys):
   resp = getvals( {"q":"supportreqs"})
   print(resp.json())
   json = resp.json()
   assert len(json) > 0
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

# Visitor_actions
def test_post_register(capsys):
   customer_email='bhavana@yahoo.com'
   customer_name='Bhavana baby'
   password='baby'
   body = {
			"verb": "register",
			"customer_name" : customer_name,
			"customer_email" :customer_email,
			"password" : password,
	};
   resp = postreq(body)
   json = resp.json()
   print(resp.json())
   assert json['code'] == 200


# Visitor_actions
def test_post_deploy_instance(capsys):
   ipaddr = '8.8.8.8'
   today = int(DT.datetime.now().timestamp())
   ipcntry =  'US' 
   name = 'Bhavana Actress'
   email = 'girish@yahoo.com'
   product = 'SpamCheetah'
   body = {
			"verb": "deploy_instance",
			"Created" : today,
			"IPAddress" : ipaddr,
			"Country" : ipcntry,
			"Product" : product,
			"Name" : name,
			"Email" : email
	};
   resp = postreq(body)
   json = resp.json()
   print(resp.json())
   assert json['code'] == 201


# Visitor_actions
def test_post_download_product(capsys):
   ipaddr = '8.8.8.8'
   today = int(DT.datetime.now().timestamp())
   created = today
   ipcntry =  'US' 
   product = 'SpamCheetah'
   name = 'Bhavana Actress'
   email = 'girish@yahoo.com'
   body = {
			"verb": "download_product",
			"Created" : created,
			"IPAddress" : ipaddr,
			"Country" : ipcntry,
			"Product" : product,
			"Email" : email
	};
   resp = postreq(body)
   json = resp.json()
   print(resp.json())
   assert json['code'] == 201


# Visitor_actions
def test_post_create_invoice(capsys):
   customer_uid = 'bhavana@yahoo.com'
   customer_name = 'Latha beauty'
   payment_id = 'x34234'
   price = 2345
   paid = True
   months = 3
   prod = 'SpamCheetah'
   lic_type = 'standard'
   max = "40 mailids"
   hosted = True 
   body = {
			"verb": "create_invoice",
			"customer_email" : customer_uid,
			"customer_name" : customer_name,
			"payment_id" : payment_id,
			"months" : months,
			"price" : price,
			"hosted" : hosted,
			"max": max,
			"license_type" : lic_type,
			"paid" : paid,
	};
   resp = postreq(body)
   json = resp.json()
   print(resp.json())
   assert json['code'] == 201

# Visitor_actions
def test_post_create_issue(capsys):
   name = 'Girish Baby'
   email = 'fire@yahoo.in'
   sub = 'have fun'
   msg = 'how r u'
   cat = choice(['technical',
        'general',
        'feedback',
        'accounting'])
   body = {
			"verb": "create_issue",
			"Name" : name,
			"Email" : email,
			"Message" : msg,
			"Subject" : sub,
			"Category" : cat
	};
   resp = postreq(body)
   json = resp.json()
   print(resp.json())
   assert json['code'] == 201


# Visitor_actions
def test_post_sendMsg(capsys):
   name = 'Girish Baby'
   email = 'fire@yahoo.in'
   sub = 'have fun'
   msg = 'how r u'

   cat = choice(['standard',
           'heavyduty',
           'cloud',
           'general',
           'feedback',
           'accounting'])
   body = {
			"verb": "sendMsg",
			"Name" : name,
			"Email" : email,
			"Message" : msg,
			"Subject" : sub,
			"Category" : cat
	};
   resp = postreq(body)
   print(resp.json())
   json = resp.json()
   assert json['code'] == 201


# Visitor_actions
def test_post_hostedReq(capsys):
   name = 'Girish Baby'
   email = 'fire@yahoo.in'
   product = choice(["standard", "heavyduty"])
   body = {
			"verb": "hostedReq",
			"Name" : name,
			"Email" : email,
			"Product" : product,
	};
   resp = postreq(body)
   print(resp.json())
   json = resp.json()
   assert json['code'] == 201


# search
def test_get_search(capsys):
   term = choice(["mango", 'apple', 'deploy'])
   resp = getsearch({"searchterm":term})
   json = resp.json()
   assert json['code'] == 200
   print(resp.json())
 

# userCredMgr
def test_post_login_success(capsys):
   email = 'bhavana@yahoo.com'
   passwd = 'baby'
   body = {
		"q": "login",
		"customer_email": email,
		"password" : passwd
	};
   resp = postcred(body)
   json = resp.json()
   print(resp.json())
   assert json['code'] == 201


# userCredMgr
def test_post_login_failure(capsys):
   email = 'bhavana@yahoo.com'
   passwd = 'apple'
   body = {
		"q": "login",
		"customer_email": email,
		"password" : passwd
	};
   resp = postcred(body)
   json = resp.json()
   json = resp.json()
   assert json['code'] == 404



# userCredMgr
def test_post_changepass(capsys):
   email = 'girish@yahoo.in'
   passwd = 'mango'
   body = {
			"customer_email": email,
			"new_password" : passwd,
	};
   resp = postcred(body)
   json = resp.json()
   print(resp.json())
   assert json['code'] == 200

# forgotPassword
def test_get_sendForgotLink(capsys):
   email = 'bhavana@yahoo.com'
   resp = getforgot({"action":"send_forgotLink", "email": email})
   json = resp.json()
   assert json['code'] == 200
   print(resp.json())
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


# forgotPassword
def test_get_identFromLink(capsys):
   id = '55ca6286e3e4f4fba5d0448333fa99fc5a404a73'
   resp = getforgot({"action":"fetch_ident_fromLink", "idstring": id})
   json = resp.json()
   assert json['code'] == 200
   print(resp.json())
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

# mail_test_rw
@pytest.mark.skip()
def test_get_mailtest(capsys):
   permurl = 'http://spamcheetah.com/XXSDFSAf'
   resp = getmailtest({"permurl": url})
   json = resp.json()
   assert json['code'] == 200
   print(resp.json())
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

# mail_test_rw

@pytest.mark.skip()
def test_post_mailtest(capsys):
   toid = 'mango@yahoo.com'
   generalinfo = '<h1>General</h1>'
   rspamdverdict = '<h2>rspamd</h2>'
   viruschecks = '<h3>Virus checks</h3>'
   attachscan = '<h3>Attachment scanning</h3>'
   mail_body = '<p> I am a nice man</p>'
   mail_header = 'What is up?'
   summary = '<h2> All tests passed</h2>'
   urlchecks = '<h3> No URLs found</h3>'

   body = {
		"toid" : toid,
		"generalinfo" : generalinfo,
		"rspamdverdict" : rspamdverdict,
		"viruschecks": viruschecks,
		"attachmentscanning": attachscan,
		"mail_body": mail_body,
		"mail_header": mail_header,
		"summary": summary,
		"urlchecks": urlchecks
	};
   resp = postmailtest(body)
   json = resp.json()
   print(resp.json())
   assert json['code'] == 200

